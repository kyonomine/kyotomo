module Potepan
  class ProductsController < ApplicationController
    MAX_RELATED_PRODUCT_COUNT = 4

    def show
      @product = Spree::Product.find(params[:id])
      @related_products = @product.related_products.limit(MAX_RELATED_PRODUCT_COUNT)
    end
  end
end
