RSpec.describe 'Potepan::ProductDecorator', type: :model do
  describe 'related_products_test' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:product) { create(:product, name: 'product', taxons: [taxon]) }
    let(:unrelated_product) { create(:product, taxons: [taxon]) }

    it '商品詳細ページの商品が関連商品には含まれないことを確認' do
      expect(product.related_products).not_to include product
    end

    it '関連しない商品が含まれないことを確認' do
      expect(product.related_products).not_to include unrelated_product.taxons
    end
  end
end
