require 'rails_helper'

RSpec.describe 'Products::Request', type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'レスポンスで200が返ってくることを確認' do
      expect(response).to have_http_status(200)
    end

    it '商品名が含まれていることことを確認' do
      expect(response.body).to include product.name
    end

    it '商品価格が含まれていることを確認' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品説明が含まれていることを確認' do
      expect(response.body).to include product.description
    end

    it '関連商品が4つ表示されていること' do
      expect(controller.instance_variable_get('@related_products').count).to eq 4
    end
  end
end
