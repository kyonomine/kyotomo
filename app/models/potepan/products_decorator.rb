module Potepan
  module ProductDecorator
    def related_products
      Spree::Product.in_taxons(taxons)
                    .where.not(id: id)
                    .distinct
                    .includes(master: %i[default_price images])
    end

    Spree::Product.prepend self
  end
end
