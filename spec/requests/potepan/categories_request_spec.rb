require 'rails_helper'

RSpec.describe 'Potepan::Categories', type: :request do
  describe 'GET #show' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'アクセスに成功すること' do
      expect(response).to have_http_status(:success)
    end

    it 'レスポンスで200が返ってくること' do
      expect(response).to have_http_status(200)
    end

    it '商品名が含まれていること' do
      expect(response.body).to include product.name
    end

    it '商品価格が含まれていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it 'カテゴリー名が含まれていること' do
      expect(response.body).to include taxonomy.name
    end

    it '商品の分類名が含まれていること' do
      expect(response.body).to include taxon.name
    end

    it '商品数が含まれていること' do
      expect(response.body).to include taxon.products.count.to_s
    end
  end
end
