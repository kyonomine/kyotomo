# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    subject { get :show, params: { id: product.id } }

    let(:product) { create(:product) }

    it 'レスポンスの成功確認' do
      subject
      expect(response).to be_successful
    end

    it '@productへの受け渡し確認' do
      subject
      expect(assigns(:product)).to eq product
    end

    it 'showテンプレート' do
      subject
      expect(response).to render_template :show
    end
  end
end
