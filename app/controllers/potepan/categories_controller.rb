module Potepan
  class CategoriesController < ApplicationController
    def show
      @taxonomies = Spree::Taxonomy.includes(:root)
      @taxon = Spree::Taxon.find(params[:id])
      @products = @taxon.all_products.includes(master: %i[images default_price])
    end
  end
end
