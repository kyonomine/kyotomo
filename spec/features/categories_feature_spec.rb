require 'rails_helper'

RSpec.feature 'Categories_features', type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario 'カテゴリーページで表示されているコンテンツ' do
    expect(page).to have_current_path potepan_category_path(taxon.id)
    expect(page).to have_title "#{taxon.name} | BIGBAG Store"
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content taxon.products.count
  end

  scenario 'sidebarの機能確認' do
    within '.sideBar' do
      click_on taxonomy.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  scenario '商品の分類で、クリックしたカテゴリーページへ移動' do
    click_on taxon.name
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario '商品名をクリックすると、商品の詳細ページへ移動' do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
